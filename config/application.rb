$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'app'))
$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'boot'

Bundler.require :default, ENV['RACK_ENV']

OTR::ActiveRecord.configure_from_file! 'config/database.yml'
Dir[File.expand_path('../../app/models/**/*.rb', __FILE__)].each { |file| require file }
Dir[File.expand_path('../../app/api/**/*.rb', __FILE__)].each { |file| require file }
Dir[File.expand_path('../../app/services/**/*.rb', __FILE__)].each { |file| require file }
Dir[File.expand_path('../../app/serializers/**/*.rb', __FILE__)].each { |file| require file }

require 'api/base'
