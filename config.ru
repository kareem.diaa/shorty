# frozen_string_literal: true

require File.expand_path('../config/environment', __FILE__)
require 'rack/cors'
require 'grape_logging'

use Rack::Cors do
  allow do
    origins '*'
    resource '*', headers: :any, methods: :any
  end
end

use OTR::ActiveRecord::ConnectionManagement
run Rack::Cascade.new([API::Base])
