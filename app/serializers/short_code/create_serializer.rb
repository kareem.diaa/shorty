# frozen_string_literal: true

require 'models/short_code'

module Serializers
  module ShortCode
    class CreateSerializer < ActiveModel::Serializer
      attribute :shortcode

      def shortcode
        object.target_url
      end
    end
  end
end
