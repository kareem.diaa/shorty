# frozen_string_literal: true

require 'services/actions/base'
require 'serializers/short_code/stats_serializer'

module Actions
  module ShortCode
    class Stats < Actions::Base
      ErrorMessages = {
        invalid_short_code: 'The shortcode cannot be found in the system'
      }.freeze
      def initialize(params)
        @target_url = params[:shortcode]
        fetch_target_url
        super
      end

      def perform
        return respond_with_failure(404, ErrorMessages[:invalid_short_code]) if @short_code.nil?
        respond_with_success(serialize_output, 200)
      end

      private

      def fetch_target_url
        @short_code = ::ShortCode.find_by(target_url: @target_url)
      end

      def serialize_output
        ActiveModelSerializers::SerializableResource.new(
          @short_code,
          serializer: ::Serializers::ShortCode::StatsSerializer,
          adapter: :json
        ).serializable_hash[:short_code]
      end
    end
  end
end
