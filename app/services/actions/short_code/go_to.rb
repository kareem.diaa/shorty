# frozen_string_literal: true

require 'services/actions/base'
require 'models/short_code'

module Actions
  module ShortCode
    class GoTo < Actions::Base
      ErrorMessages = {
        invalid_short_code: 'The shortcode cannot be found in the system'
      }.freeze

      def initialize(params)
        @target_url = params[:shortcode]
        fetch_target_url
        super
      end

      def perform
        return respond_with_failure(404, ErrorMessages[:invalid_short_code]) if @short_code.nil?
        respond_with_success(@short_code.original_url, 302) if save_redirect!
      end

      private

      def fetch_target_url
        @short_code = ::ShortCode.find_by(target_url: @target_url)
      end

      def save_redirect!
        @short_code.redirects.create!
      end
    end
  end
end
