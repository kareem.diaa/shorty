# frozen_string_literal: true

require 'exceptions'

module Actions
  class Base
    attr_accessor :response_code, :response_body
    def initialize(*_args)
      @response_code = 200
      @response_body = nil
    end

    def respond_with_success(body, code = 200)
      @response_code = code
      @response_body = body
      @failed = false
    end

    def respond_with_failure(code = 500, error = 'Something went wrong')
      send("raise_#{code}", error)
    end

    def raise_400(e)
      raise Shorty::Exceptions::BadRequest, e
    end

    def raise_409(e)
      raise Shorty::Exceptions::Conflict, e
    end

    def raise_422(e)
      raise Shorty::Exceptions::UnprocessableEntity, e
    end

    def raise_404(e)
      raise Shorty::Exceptions::NotFound, e
    end

    def raise_500(e)
      raise Shorty::Exceptions::InternalError, e
    end

    def self.perform(*args)
      action = new(*args)
      action.perform
      action
    end
  end
end
