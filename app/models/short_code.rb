# frozen_string_literal: true

require 'uri'

# Main Model that holds source and target urls
class ShortCode < ActiveRecord::Base
  ############################################## Relations ########################################
  has_many :redirects
  ############################################## Relations ########################################

  ############################################## Validations ########################################
  validates :target_url, :original_url, presence: true
  validates_format_of :target_url, with: /\A[0-9a-zA-Z_]{6}\z/
  validates_format_of :original_url, with: URI::DEFAULT_PARSER.make_regexp
  validates :target_url, uniqueness: true
  ############################################## Validations ########################################
end
