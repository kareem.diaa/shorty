# frozen_string_literal: true

require_relative 'short_codes'
require_relative 'error_handler'

module API
  module V1
    class Base < Grape::API
      version 'v1'
      include API::V1::ErrorHandler
      mount API::V1::ShortCodes
    end
  end
end
