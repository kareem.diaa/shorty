# frozen_string_literal: true

require 'services/actions/short_code/create'

module API
  module V1
    class ShortCodes < Grape::API
      namespace :shorten do
        desc 'Endpoint to shorten your url'
        params do
          requires :url, type: String, regexp: /^\S+$/, desc: 'Url to shorten', as: :original_url
          optional :shortcode, type: String, regexp: /^\S+$/, desc: 'Preferential shortcode', as: :target_url
        end
        post do
          action = Actions::ShortCode::Create.perform(declared(params))
          status action.response_code
          action.response_body
        end
      end

      route_param :shortcode do
        desc 'Endpoint to redirect to the shortened code'
        get do
          action = Actions::ShortCode::GoTo.perform(params)
          header('Location', action.response_body)
          status(action.response_code)
        end

        namespace :stats do
          desc 'Endpoint to retrieve stats about a shortened code'
          get do
            action = Actions::ShortCode::Stats.perform(params)
            status action.response_code
            action.response_body
          end
        end
      end
    end
  end
end
