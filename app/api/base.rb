# frozen_string_literal: true

require_relative 'v1/base'

# Global API
module API
  # General API configuration
  class Base < Grape::API
    prefix :api
    format :json
    mount API::V1::Base
  end
end
