class CreateRedirects < ActiveRecord::Migration[5.1]
  def change
    create_table(:redirects) do |t|
      t.integer :short_code_id
      t.timestamps
    end
    add_index :redirects, :short_code_id
  end
end
