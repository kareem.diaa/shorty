# frozen_string_literal: true

FactoryBot.define do
  factory :redirect do
    short_code
  end
end
