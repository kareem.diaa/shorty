# frozen_string_literal: true

FactoryBot.define do
  factory :short_code do
    original_url Faker::Internet.url
    target_url Faker::Base.regexify(/^[0-9a-zA-Z_]{6}$/)
  end
end
