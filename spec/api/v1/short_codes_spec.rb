# frozen_string_literal: true

require 'spec_helper'

describe ShortCode do
  describe 'POST /shorten' do
    subject { post '/api/v1/shorten', url: original_url, shortcode: target_url }

    context 'when url is not present' do
      let(:original_url) { nil }
      let(:target_url) { nil }

      it 'should return an error with status 400' do
        subject
        expect_status(400)
        expect_json(error: 'url is not present')
      end
    end

    context 'when shortcode is invalid' do
      let(:original_url) { 'http://www.example.com' }
      let(:target_url) { 'example' }

      it 'should return an error with status 422' do
        subject
        expect_status(422)
        expect_json(error: 'The shortcode fails to meet the following regexp: ^[0-9a-zA-Z_]{6}$.')
      end
    end

    context 'when shortcode is already exists' do
      let(:original_url) { 'http://www.example.com' }
      let(:target_url) { 'kareem' }

      before { create(:short_code, target_url: 'kareem') }

      it 'should return an error with status 409' do
        subject
        expect_status(409)
        expect_json(error: 'The the desired shortcode is already in use. Shortcodes are case-sensitive.')
      end
    end

    context 'when api documentation is followed' do
      let(:original_url) { 'http://www.example.com' }
      let(:target_url) { 'kareem' }

      it 'should return a newly create short_code and status 201' do
        subject
        expect_status(201)
        expect_json(shortcode: 'kareem')
      end
    end
  end

  describe 'GET /:shortcode' do
    subject { get "/api/v1/#{target_url}" }

    context 'when target_url does not exist in database' do
      let(:target_url) { 'kareem' }

      it 'should return an error with status 404' do
        subject
        expect_status(404)
        expect_json(error: 'The shortcode cannot be found in the system')
      end
    end

    context 'when api documentation is followed' do
      let(:target_url) { 'kareem' }
      let!(:short_code) { create(:short_code, target_url: 'kareem') }

      it 'should redirect to the original url' do
        subject
        expect_status(302)
        expect_header('Location', short_code.original_url)
      end
    end
  end

  describe 'GET /:shortcode/stats' do
    subject { get "/api/v1/#{target_url}/stats" }

    context 'when target_url does not exist in database' do
      let(:target_url) { 'kareem' }

      it 'should return an error with status 404' do
        subject
        expect_status(404)
        expect_json(error: 'The shortcode cannot be found in the system')
      end
    end

    context 'when api documentation is followed' do
      let(:target_url) { 'kareem' }
      let!(:short_code) { create(:short_code, target_url: 'kareem') }

      context 'and short_code has no redirects' do
        it 'should return 200 and short_code stats' do
          subject
          expect_status(200)
          expect_json(
            startDate: short_code.created_at.iso8601,
            redirectCount: short_code.redirects.count
          )
        end
      end

      context 'and short_code has previous redirects' do
        let!(:redirect)   { create(:redirect, short_code: short_code) }
        it 'should return 200 and short_code stats' do
          subject
          expect_status(200)
          expect_json(
            startDate: short_code.created_at.iso8601,
            redirectCount: short_code.redirects.count,
            lastSeenDate: short_code.redirects.last.created_at.iso8601
          )
        end
      end
    end
  end
end
