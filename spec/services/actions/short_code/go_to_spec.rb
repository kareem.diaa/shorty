# frozen_string_literal: true

require 'spec_helper'

describe Actions::ShortCode::GoTo do
  subject { described_class.perform(params) }

  let(:params) do
    {
      shortcode: target_url
    }
  end

  context 'when target_url does not exist' do
    let(:target_url) { 'kareem' }
    it 'should raise 404 NotFound' do
      expect { subject }.to raise_error(Shorty::Exceptions::NotFound)
    end
  end

  context 'when all inputs are valid' do
    let(:target_url) { 'kareem' }

    let!(:saved_short_code) { create(:short_code, target_url: 'kareem') }

    it 'should return status 302 and original_url as body' do
      expect(subject.response_code).to eq(302)
      expect(subject.response_body).not_to eq(nil)
      expect(subject.response_body).to eq(saved_short_code.original_url)
    end

    it 'creates a redirect object' do
      expect { subject }.to change { saved_short_code.redirects.count }.from(0).to(1)
    end
  end
end
